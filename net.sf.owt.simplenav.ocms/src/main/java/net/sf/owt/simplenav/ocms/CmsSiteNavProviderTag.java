/*
 * Id   : $Id$
 * This file is part of the SimpleNav project.
 * URL: https://sourceforge.net/projects/simplenav/ 
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * For further information about OpenCms, please see the
 * project website: http://www.opencms.org
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package net.sf.owt.simplenav.ocms;

import java.util.List;

/**
 * An <code>INavigationProvider</code> wich provides an OpenCms site navigation.
 *
 * @author Sebastian Himberger
 * @see org.opencms.jsp.CmsJspNavBuilder#getSiteNavigation()
 */
public class CmsSiteNavProviderTag extends ACmsBaseNavProviderTag {

    /** The default end level */
    public static final int C_DEFAULT_ENDLEVEL = -1;
    
    /** The max end level */
    public static final int C_MAX_ENDLEVEL = 999;
    
    /** The level where to end the navigation */
    private int endLevel;
    
    /**
     * Constructs a new instance and initializes the default values 
     */
    public CmsSiteNavProviderTag() {
    	resetTag();
    }
    
    /**
     * Resets the tag members
     */
    protected void resetTag() {
    	super.resetTag();
    	endLevel = CmsSiteNavProviderTag.C_DEFAULT_ENDLEVEL;
    }
    
	/**
	 * Creates the OpenCms navigation used by this tag.
	 * 
	 * @see net.sf.owt.simplenav.ocms.ACmsBaseNavProviderTag#createCmsNavigation()
	 * @see org.opencms.jsp.CmsJspNavBuilder#getSiteNavigation()
	 */
	protected List createCmsNavigation() {
		if (targetResourceName == null) {
			if (endLevel == -1) {
				return cmsAction.getNavigation().getSiteNavigation();
			} 
			return cmsAction.getNavigation().getSiteNavigation("/",endLevel);
		
		} else {
			if (endLevel == -1) {
				return cmsAction.getNavigation().getSiteNavigation(cmsTargetNavElem.getResourceName(), CmsSiteNavProviderTag.C_MAX_ENDLEVEL);
			} 
			return cmsAction.getNavigation().getSiteNavigation(cmsTargetNavElem.getResourceName(),endLevel);
			
		}
	}
    
	/**
	 * Returns the currently set end level
	 * @return the currently set end level
	 */
    public int getEndLevel() {
        return endLevel;
    }
    
    /**
     * Sets the level where to end the navigation
     * @param level The level where to end the navigation
     */
    public void setEndLevel(int level) {
        endLevel = level;
    }


}
