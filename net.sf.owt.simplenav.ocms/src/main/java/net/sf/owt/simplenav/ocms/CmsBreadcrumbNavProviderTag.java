/*
 * Id   : $Id$
 * This file is part of the SimpleNav project.
 * URL: https://sourceforge.net/projects/simplenav/ 
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * For further information about OpenCms, please see the
 * project website: http://www.opencms.org
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package net.sf.owt.simplenav.ocms;

import java.util.List;

/**
 * Implementation of <code>INavigationProvider</code> wich provides an OpenCms breadcrumb navigation.
 *  
 * @author Sebastian Himberger
 * @see org.opencms.jsp.CmsJspNavBuilder#getNavigationBreadCrumb(java.lang.String, int, int, boolean)
 */
public class CmsBreadcrumbNavProviderTag extends ACmsBaseNavProviderTag {

	/** The default navigation level to start with */
	private static final int C_DEFAULT_STARTLEVEL = 0;

	private static final int C_DEFAULT_ENDLEVEL = -1;
	
	private static final boolean C_DEFAULT_CURRENTFOLDER = true;
	
	private static final boolean C_DEFAULT_CURRENTFILE = true;
	
	/** Include the current folder in the navigation */
	private boolean currentFolder;
	
	/** Include the current file into the navigation */
	private boolean currentFile;
	
	/** The level where to start the navigation */
	private int startLevel;
	
	/** The level where to end the navigation 
	 * @see net.sf.owt.simplenav.ocms.CmsTreeNavProvider#C_DEFAULT_ENDLEVEL 
	 */
	private int endLevel;	

	public CmsBreadcrumbNavProviderTag() {
		resetTag();
	}

	protected void resetTag() {
		super.resetTag();
		endLevel = CmsBreadcrumbNavProviderTag.C_DEFAULT_ENDLEVEL;
		startLevel = CmsBreadcrumbNavProviderTag.C_DEFAULT_STARTLEVEL;
		currentFolder = CmsBreadcrumbNavProviderTag.C_DEFAULT_CURRENTFOLDER;
		currentFile = CmsBreadcrumbNavProviderTag.C_DEFAULT_CURRENTFILE;
	}
	
	/**
	 * @see net.sf.owt.simplenav.ocms.ACmsBaseNavProviderTag#createCmsNavigation()
	 */
	protected List createCmsNavigation() {

		List navigation;
		String folder = folder = cmsTargetNavElem.getResourceName(); 
		CmsNavigationHelper navHelper = new CmsNavigationHelper(cmsAction); 
		
		// if the target is not a folder use the parent folder name
		if (!cmsTargetNavElem.isFolderLink()) {
			folder = cmsTargetNavElem.getParentFolderName();
		}		
		
		if (endLevel == -1) {
			// get navigation using the current level as end level
			navigation = cmsAction.getNavigation().getNavigationBreadCrumb(folder ,startLevel, cmsTargetNavElem.getNavTreeLevel(), currentFolder);
			if (currentFile && !cmsTargetNavElem.isFolderLink() &&
				cmsTargetNavElem.getNavTreeLevel() >= startLevel &&
				!navHelper.isDefaultFile(folder, cmsTargetNavElem.getResourceName()) ) {
				/* if we want to include the current file and this is not located
				 * in a level below the startlevel and is not the default file of the parent folder
				 * append it to the navigation list 
				 */
				navigation.add(cmsTargetNavElem);				
			}
		} else {
			// get navigation using the specified level as end level
			navigation = cmsAction.getNavigation().getNavigationBreadCrumb(folder ,startLevel, endLevel,currentFolder);
			if (currentFile && !cmsTargetNavElem.isFolderLink() &&
				cmsTargetNavElem.getNavTreeLevel() >= startLevel &&
				!navHelper.isDefaultFile(folder, cmsTargetNavElem.getResourceName()) ) {
				/* if we want to include the current file and this is not located
				 * in a level below/above the startlevel/endlevel and is not the default file of the parent folder
				 * append it to the navigation list
				 */
				navigation.add(cmsTargetNavElem);
			}
		}		
		return navigation;
	}
	
	/**
	 * Returns the level where to end the navigation.
	 * 
	 * @return Returns the end level.
	 */
	public int getEndLevel() {
		return endLevel;
	}
	/**
	 * Sets the level where to end the navigation.
	 *  
	 * @param endLevel The end level to set.
	 */
	public void setEndLevel(int endLevel) {
		this.endLevel = endLevel;
	}
	/**
	 * Returns the level where to start the navigation.
	 * 
	 * @return Returns the start level.
	 */
	public int getStartLevel() {
		return startLevel;
	}
	/**
	 * Sets the level where to start the navigation.
	 * 
	 * @param startLevel The start level to set.
	 */
	public void setStartLevel(int startLevel) {
		this.startLevel = startLevel;
	}	
	
	/**
	 * @return Returns the currentFolder.
	 */
	public boolean getCurrentFolder() {
		return currentFolder;
	}
	/**
	 * @param currentFolder The currentFolder to set.
	 */
	public void setCurrentFolder(boolean currentFolder) {
		this.currentFolder = currentFolder;
	}
	
	/**
	 * @return <code>true</code> If the current file should be included
	 */
	public boolean getCurrentFile() {
		return currentFile;
	}
	
	public void setCurrentFile(boolean isIncluded) {
		currentFile = isIncluded;
	}
}
