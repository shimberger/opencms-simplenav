/*
 * Id   : $Id$
 * This file is part of the SimpleNav project.
 * URL: https://sourceforge.net/projects/simplenav/ 
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * For further information about OpenCms, please see the
 * project website: http://www.opencms.org
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package net.sf.owt.simplenav.ocms;

import java.util.List;

/**
 * An implementation of <code>ACmsBaseNavProviderTag</code> wich implements
 * the folder navigation methods of the <code>CmsJspNavBuilder</code>
 * 
 * @author Sebastian Himberger
 */
public class CmsFolderNavProviderTag extends ACmsBaseNavProviderTag {

	/** The default level to use */
	public static final int C_DEFAULT_LEVEL = -1;	
	
	/** The level to use */
    private int level;

    /**
     * Constructs a new instance and sets default values.
     */
    public CmsFolderNavProviderTag() {
    	resetTag();
    }
    
    /**
     * Resets the tag members to take care of pooling.
     */
    protected void resetTag() {
    	super.resetTag();
    	level = CmsFolderNavProviderTag.C_DEFAULT_LEVEL;
    }
    
    /**
     * Creates the navigation List used by <code>ACmsBaseNavProviderTag</code>
     * @see net.sf.simplenav.ocms.ABaseNavProviderTag
     */
	protected List createCmsNavigation() {
		// if level is set to -1 use the current level
		if (level == -1) {
			return cmsAction.getNavigation().getNavigationForFolder(cmsTargetNavElem.getResourceName());
		} 
		return cmsAction.getNavigation().getNavigationForFolder(cmsTargetNavElem.getResourceName(),level);
	}
	
	/**
	 * Sets the level parameter.
	 * @param level The level to use
	 */
	public void setLevel(int level) {
		this.level = level;
	}
	
	/** 
	 * Gets the level parameter.
	 * @return The currently used level
	 */
	public int getLevel() {
		return level;
	}
	
}
