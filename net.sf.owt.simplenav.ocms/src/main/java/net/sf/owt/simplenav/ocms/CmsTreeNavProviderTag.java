/*
 * Id   : $Id$
 * This file is part of the SimpleNav project.
 * URL: https://sourceforge.net/projects/simplenav/ 
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * For further information about OpenCms, please see the
 * project website: http://www.opencms.org
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package net.sf.owt.simplenav.ocms;

import java.util.List;

/**
 * Implementation of <code>ACmsDefaultNavigationProvider</code> wich uses the tree
 * navigation provided by the <code>CmsJspNavBuilder</code>.
 *
 * @author Sebastian Himberger
 * @see org.opencms.jsp.CmsJspNavBuilder#getTreeNavigation()
 */
public class CmsTreeNavProviderTag extends ACmsBaseNavProviderTag  {

	/** The default navigation level to start with */
	private static final int C_DEFAULT_STARTLEVEL = 0;

	/** The default end level */
	private static final int C_DEFAULT_ENDLEVEL = -1;
	
	/** The level where to start the navigation */
	private int startLevel;
	
	/** The level where to end the navigation 
	 * @see net.sf.simplenav.ocms.CmsTreeNavProvider#C_DEFAULT_ENDLEVEL 
	 */
	private int endLevel;
  
	
	public CmsTreeNavProviderTag() {
		resetTag();
	}	
	
	/**
	 * Resets the tag members 
	 */
	protected void resetTag() {
		super.resetTag();
		startLevel = CmsTreeNavProviderTag.C_DEFAULT_STARTLEVEL;
		endLevel = CmsTreeNavProviderTag.C_DEFAULT_ENDLEVEL;
	}
	
	/**
	 * Returns an OpenCms tree navigation.
	 * 
	 * @return A list of <code>CmsJspNavElement</code>s.
	 * @see org.opencms.jsp.CmsJspNavBuilder#getTreeNavigation()
	 */
	protected List createCmsNavigation() {
		if (endLevel == -1) {
			// if level is -1 use the level of the currently requested resource
			return cmsAction.getNavigation().getNavigationTreeForFolder(startLevel, cmsAction.getNavigation().getNavigationForResource(cmsTargetNavElem.getResourceName()).getNavTreeLevel());
		}
		return cmsAction.getNavigation().getNavigationTreeForFolder(startLevel, endLevel);		
	}

	/**
	 * Returns the level where to end the navigation.
	 * 
	 * @return Returns the end level.
	 */
	public int getEndLevel() {
		return endLevel;
	}
	/**
	 * Sets the level where to end the navigation.
	 *  
	 * @param endLevel The end level to set.
	 */
	public void setEndLevel(int endLevel) {
		this.endLevel = endLevel;
	}
	/**
	 * Returns the level where to start the navigation.
	 * 
	 * @return Returns the start level.
	 */
	public int getStartLevel() {
		return startLevel;
	}
	/**
	 * Sets the level where to start the navigation.
	 * 
	 * @param startLevel The start level to set.
	 */
	public void setStartLevel(int startLevel) {
		this.startLevel = startLevel;
	}

}
