/*
 * Id   : $Id$
 * This file is part of the SimpleNav project.
 * URL: https://sourceforge.net/projects/simplenav/ 
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * For further information about OpenCms, please see the
 * project website: http://www.opencms.org
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package net.sf.owt.simplenav.ocms;

import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.BodyContent;
import javax.servlet.jsp.tagext.BodyTagSupport;

import net.sf.owt.simplenav.core.INavElement;
import net.sf.owt.simplenav.core.INavigationProvider;
import net.sf.owt.simplenav.core.NavElementBean;

import org.opencms.file.CmsFile;
import org.opencms.file.CmsProperty;
import org.opencms.file.CmsPropertyDefinition;
import org.opencms.file.CmsResource;
import org.opencms.file.types.CmsResourceTypePointer;
import org.opencms.i18n.CmsMessages;
import org.opencms.jsp.CmsJspActionElement;
import org.opencms.jsp.CmsJspNavElement;
import org.opencms.main.CmsException;


/**
 * Basic class for building OpenCms based <code>INavigationProvider</code>s. To implement a
 * new Provider the method <code>createCmsNavigation()</code> has to be implemented to return
 * a list of <code>CmsJspNavElements</code> as provided by the <code>CmsJspNavBuilder</code>.
 * The rest of work is done by this implementation.
 *
 * @author Sebastian Himberger
 * @see org.opencms.jsp.CmsJspNavBuilder
 * @see net.sf.owt.simplenav.core.INavigationProvider
 */
public abstract class ACmsBaseNavProviderTag extends BodyTagSupport implements INavigationProvider {

	/** Object to access OpenCms functions */
	protected CmsJspActionElement cmsAction;	

	/** The navigation provided by this tag */
	protected List navigation;
	
	/** Helper object to determine default files, waypoints, etc. */
	protected CmsNavigationHelper cmsNavHelper;
	
	/** The OpenCms NavElement for wich to build the navigation */
	protected CmsJspNavElement cmsTargetNavElem;    
    
	/** The target resource name */
	protected String targetResourceName;

	/** If external links should be generated for pointers */
	protected boolean generateExternalLinks = true;
	
    /** Indicates if this provider must force the use of the alternative caption */
    protected boolean useTitleInsteadNavText;
    
	/**
	 * Initilaizes the required objects. Overriding classes should call this method for proper
	 * functioning.
	 */
	protected void doInitTag() {
		cmsAction = new CmsJspActionElement(pageContext, (HttpServletRequest) pageContext.getRequest(), (HttpServletResponse) pageContext.getResponse());
		cmsNavHelper = new CmsNavigationHelper(cmsAction);
		
		// use current page as target if no other target is specified 
		if (targetResourceName == null) {
			cmsTargetNavElem = cmsAction.getNavigation().getNavigationForResource();
		} else {
			cmsTargetNavElem = cmsAction.getNavigation().getNavigationForResource(targetResourceName);
		}
	}
	
	/**
	 * Resets the class memebers because of pooling
	 */
	protected void resetTag() {
		targetResourceName = null;
		generateExternalLinks = true;
		navigation = null;
	}
	
	/**
	 * Builds the list of <code>INavElement</code>s from the OpenCms navigation list.
	 * 
	 * @return EVAL_BODY_BUFFERED
	 */
	public int doStartTag() {		
        doInitTag(); 
        List cmsNavigation = createCmsNavigation(); // get OpenCms navigation used by this class
		CmsJspNavElement cmsNavElement;        
		INavElement navElement;
        
		// if we have a navigation to process
		if (cmsNavigation != null && cmsNavigation.size() > 0) {
			navigation = new LinkedList();
			
			// loop through OpenCms navigation to create navigation list usable by navigation tags
			for (Iterator i = cmsNavigation.iterator();i.hasNext();) {
				
				cmsNavElement = (CmsJspNavElement) i.next();
				navElement = new NavElementBean();
				
				// Set attributes of the navigation element
				navElement.setCaption(getCaption(cmsNavElement));
				navElement.setUrl(getNavigationLink(cmsNavElement.getResourceName(),generateExternalLinks));
				navElement.setNode(cmsNavElement.isFolderLink());
				navElement.setLevel(cmsNavElement.getNavTreeLevel());
                navElement.setImageUrl(cmsNavElement.getNavImage());
				
				// if the current resourcename is equal to the requested resourcename OR 
				// the current resource is a folder AND the requested resource is the default file of this folder
				if ( cmsTargetNavElem.getResourceName().equals(cmsNavElement.getResourceName()) || 
					 ( cmsNavElement.isFolderLink() &&  cmsNavHelper.isDefaultFile(cmsNavElement.getResourceName(),cmsTargetNavElem.getResourceName()) ) 
				   ) {
				   	navElement.setActive(true);
				} else {
					navElement.setActive(false);
				}
				
				// if the current element is a waypoint but NOT the active element
				if ( !(navElement.isActive()) && cmsNavHelper.isWaypointTo(cmsNavElement.getResourceName(),cmsTargetNavElem.getResourceName()) ) {
					navElement.setWaypoint(true);
				} else {
					navElement.setWaypoint(false);
				}			
	
				navigation.add(navElement);
			}		
		}
		
		return EVAL_BODY_BUFFERED; // proccess body
	}

	/**
	 * Returns the link for the provided resource.
	 * 
	 * @param resourceName The path to the resource
	 * @param useExternalLinks <code>true</code> If external link pointers should link to their target instead of the VFS file.
	 * @return The generated link
	 */
	protected String getNavigationLink(String resourceName,boolean useExternalLinks) {
		String link = cmsAction.link(resourceName);
		if (cmsAction.getCmsObject().existsResource(resourceName) && useExternalLinks) {
			try {
				CmsResource targetResource = cmsAction.getCmsObject().readResource(resourceName);
				if (targetResource.getTypeId() == CmsResourceTypePointer.getStaticTypeId()) {
					CmsFile navFile = cmsAction.getCmsObject().readFile(targetResource);
					String externalLink = new String(navFile.getContents());
					link = externalLink.trim();
				}
			} catch (CmsException cmse) {
				// Do nothing and build the normal link
			}
		}			
		return link;
	}
	
	/**
	 * Ouputs the evaluated body content
	 */
    public int doEndTag() throws JspException {
        BodyContent body = getBodyContent();
        try {
            // write buffered body content to JspWriter
        	body.writeOut(getPreviousOut());
            body.clear();
        } catch (IOException e) {
                throw new JspException("IOException occured: " + e.getMessage());
        } 
        resetTag();
        return SKIP_BODY;
    }
 
    private String getCaption(CmsJspNavElement navElem) {
        String caption = navElem.getNavText();
        if ( (useTitleInsteadNavText || caption.equals(CmsMessages.formatUnknownKey(CmsPropertyDefinition.PROPERTY_NAVTEXT))) ) {
            try {
                CmsProperty title = cmsAction.getCmsObject().readPropertyObject(navElem.getResourceName(),CmsPropertyDefinition.PROPERTY_TITLE, false);             
                if (title != null && !title.isNullProperty()) {
                    caption = title.getValue();
                } else {
                    caption = CmsMessages.formatUnknownKey(CmsPropertyDefinition.PROPERTY_NAVTEXT);                    
                }
            } catch (CmsException e) {
                
            }
        }    
        return caption;
    }   
    
  
    /**
     * @return The list of <code>INavElement</code>s.
     * @see net.sf.owt.simplenav.core.INavElement
     * @see net.sf.owt.simplenav.core.INavigationProvider#provideNavigation()
     */
    public List provideNavigation() {
    	return navigation;
    }
    
    /**
     * The currently assigned navigation target.
     * 
     * @return The resource name of the navigation target
     */
    public String getTargetResourceName() {
    	return targetResourceName;
    }
    
    /**
     * Sets the target resource used for this navigation. 
     * By default the currently requested resource is used as a target.
     * 
     * @param resourceName The resource name of the navigation target
     */
    public void setTargetResourceName(String resourceName) {
    	targetResourceName = resourceName;
    }
    
    public boolean isUseTitleInsteadNavText() {
        return useTitleInsteadNavText;
    }
    public void setUseTitleInsteadNavText(boolean useTitleInsteadNavText) {
        this.useTitleInsteadNavText = useTitleInsteadNavText;
    }
    
    public boolean isGenerateExternalLinks() {
		return generateExternalLinks;
	}

	public void setGenerateExternalLinks(boolean generateExternalLinks) {
		this.generateExternalLinks = generateExternalLinks;
	}

	/**
     * Creates the OpenCms navigation used to build a <code>INavigationProvider</code> compatible navigation list.
     * 
     * @return A <code>List</code> of <code>CmsJspNavElement</code>s as provided by a <code>CmsJspNavBuilder</code>
     */
    protected abstract List createCmsNavigation();
	
}
