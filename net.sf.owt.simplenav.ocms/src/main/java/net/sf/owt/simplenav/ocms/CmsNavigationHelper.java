/*
 * Id   : $Id$
 * This file is part of the SimpleNav project.
 * URL: https://sourceforge.net/projects/simplenav/ 
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * For further information about OpenCms, please see the
 * project website: http://www.opencms.org
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package net.sf.owt.simplenav.ocms;

import java.util.List;

import org.opencms.file.CmsPropertyDefinition;
import org.opencms.file.CmsResource;
import org.opencms.jsp.CmsJspActionElement;
import org.opencms.main.CmsException;
import org.opencms.main.OpenCms;

/**
 * Helper class to determine attributes of <code>CmsJspNavElement</code>s 
 * because of the interface <code>INavElement</code>. 
 * 
 * @author Sebastian Himberger
 * @see net.sf.owt.simplenav.core.INavElement
 */
public class CmsNavigationHelper {

	/** Object to access OpenCms functionality */
	private CmsJspActionElement jspAction;
	
	
	/**
	 * Constructs a new <code>CmsNavigationHelper</code>.
	 * 
	 * @param jspAction An instance of <code>CmsJspActionElement</code>
	 * @see org.opencms.jsp.CmsJspActionElement
	 */
	public CmsNavigationHelper(CmsJspActionElement jspAction) {
		this.jspAction = jspAction;
	}
	
	/**
	 * Checks if a file is the default file of a folder
	 * 
	 * @param navPath The folder to check
	 * @param fileUri The file to check
	 * @return true if the given file is the default file
	 */
	public boolean isDefaultFile(String navPath, String fileUri) {
		String folderName = CmsResource.getFolderPath(fileUri);
        if (navPath.equals(folderName)) {
            String fileName = CmsResource.getName(fileUri);
            try {
               // check if the "default-file" property was used on the folder
               String defaultFileName = this.jspAction.getCmsObject().readPropertyObject(folderName, CmsPropertyDefinition.PROPERTY_DEFAULT_FILE, false).getValue();
               if (defaultFileName != null && fileName.equals(defaultFileName)) {
               		// property was set and the requested file name matches the default file name
               		return true;    
	           } 
	        } catch (CmsException e) {
	        	// ignore exception, reading property failed
	        }
	        List defaultFileNames = OpenCms.getDefaultFiles();
	        for (int i = 0; i < defaultFileNames.size(); i++) {
	        	String currFileName = (String)defaultFileNames.get(i);
	            if (fileName.equals(currFileName)) {
	            	return true;    
	            }
	        }
        }
        return false;
     }	
	
	/**
	 * Checks if a folder is a waypoint towards a target file
	 * 
	 * @param fileUri the folder to check
	 * @param targetUri the file wich is the target
	 * @return true if the folder is a waypoint
	 */
	public boolean isWaypointTo(String fileUri, String targetUri) {
		if (targetUri.startsWith(fileUri)) {
			return true;
		}
		return false;
	}
	
}
