/*
 * Id   : $Id$
 * This file is part of the SimpleNav project.
 * URL: https://sourceforge.net/projects/simplenav/ 
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * For further information about OpenCms, please see the
 * project website: http://www.opencms.org
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package net.sf.owt.simplenav.core;

import java.util.ArrayList;
import java.util.List;

import net.sf.owt.simplenav.core.HtmlNavListTag;
import net.sf.owt.simplenav.core.NavElementBean;
import net.sf.owt.simplenav.core.NavListEntry;

import junit.framework.TestCase;

public class HtmlNavListTagTest extends TestCase {

	private static final String CAPTION_PREFIX = "Caption ";
	private static final String URL_PREFIX = "http://www.somedomain.com/file";
	
	protected void setUp() throws Exception {
		super.setUp();
	}

	public void testRenderOutput() {
			doTestRenderOutputSimple();
			doTestRenderOutputMultiLevelChange();
			doTestRenderOutputLevelChangeAtEnd();	
		
	}

	private void doTestRenderOutputSingleLevelChange() {
		HtmlNavListTag tag = new HtmlNavListTag();		
		List firstLevel01 = generateNavElements(0, 4, 1);
		List secondLevel01 = generateNavElements(0, 3, 2);
		List firstLevel02 = generateNavElements(5, 2, 1);
		
		List navList = new ArrayList();
		navList.addAll(firstLevel01);
		navList.addAll(secondLevel01);
		navList.addAll(firstLevel02);
		
		String result = tag.renderOutput(navList);		
		
		assertTrue(isBalanced(result));		
	}
	
	private void doTestRenderOutputMultiLevelChange() {
		HtmlNavListTag tag = new HtmlNavListTag();		
		List firstLevel01 = generateNavElements(0, 4, 1);
		List secondLevel01 = generateNavElements(0, 3, 2);
		List thirdLevel01 = generateNavElements(0, 2, 3);		
		List firstLevel02 = generateNavElements(5, 2, 1);
		
		List navList = new ArrayList();
		navList.addAll(firstLevel01);
		navList.addAll(secondLevel01);
		navList.addAll(thirdLevel01);
		navList.addAll(firstLevel02);
		
		String result = tag.renderOutput(navList);
		
		
		assertTrue(isBalanced(result));		
	}
	
	private void doTestRenderOutputSimple() {
		HtmlNavListTag tag = new HtmlNavListTag();		
		List simpleList = generateNavElements(0, 4, 1);
		String result = tag.renderOutput(simpleList);
		assertTrue(isBalanced(result));		
	}
	
	private void doTestRenderOutputLevelChangeAtEnd() {
		HtmlNavListTag tag = new HtmlNavListTag();	
		List lastIsFolderList = generateNavElements(0, 4, 1);
		List subList = generateNavElements(0, 3, 2);
		lastIsFolderList.addAll(subList);
		String lastIsFolderResult = tag.renderOutput(lastIsFolderList);
		assertTrue(isBalanced(lastIsFolderResult));			
	}
	
	private boolean isBalanced(String html) {
		
		html = html.replaceAll("<ul *>", "<ul>");
		
		int openUL = countStrings(html, "<ul>");
		int closeUL = countStrings(html, "</ul>");
		boolean isULBalanced = (openUL == closeUL);

		int openLI = countStrings(html, "<li>");
		int closeLI = countStrings(html, "</li>");
		boolean isLIBalanced = (openLI == closeLI);
		
		return isLIBalanced && isULBalanced;
	}
	
	private List generateNavElements(int offset, int count, int level) {
		ArrayList navElements = new ArrayList(count);
		for (int i = offset; i < count + offset; i++) {
			NavElementBean bean = new NavElementBean();
			NavListEntry entry = new NavListEntry(bean,"body");
			bean.setCaption(CAPTION_PREFIX + i);
			bean.setUrl(URL_PREFIX + i);
			bean.setLevel(level);
			navElements.add(entry);
		}
		return navElements;
	}

	private int countStrings(String haystack,String needle) {
		int occurences = 0;
		int needleLength = needle.length();
		int startIndex = haystack.indexOf(needle);
		while (startIndex != -1) {
			occurences++;
			startIndex = haystack.indexOf(needle, startIndex + needleLength);
		}
		return occurences;
	}
	
}
