/*
 * Id   : $Id$
 * This file is part of the SimpleNav project.
 * URL: https://sourceforge.net/projects/simplenav/ 
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * For further information about OpenCms, please see the
 * project website: http://www.opencms.org
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package net.sf.owt.simplenav.core;

/**
 * An navigation list entry used by <code>ANavListTag</code> for
 * easy accessing of the evaluated bodies and the associated 
 * <code>INavElement</code>s from the <code>renderOutput</code> method.
 * 
 * @author Sebastian Himberger
 */
public class NavListEntry {

	/** The navigation element used by this entry */
	private INavElement navElement;
	
	/** The evaluated body of this entry */
	private String body;
	
	/**
	 * Empty constructor for JavaBeans specs.
	 */
	public NavListEntry() {
		// NOP
	}
	
	/**
	 * Constructs a new <code>NavListEntry</code>
	 * 
	 * @param navElem The navigation element
	 * @param body The evaluated body
	 */
	public NavListEntry(INavElement navElem, String body) {
		navElement = navElem;
		this.body = body;
	}
	
	/**
	 * @return Returns the evaluated body.
	 */
	public String getBody() {
		return body;
	}
	/**
	 * @param body The body to set.
	 */
	public void setBody(String body) {
		this.body = body;
	}
	/**
	 * @return Returns the navElement.
	 */
	public INavElement getNavElement() {
		return navElement;
	}
	/**
	 * @param navElement The navElement to set.
	 */
	public void setNavElement(INavElement navElement) {
		this.navElement = navElement;
	}
}
