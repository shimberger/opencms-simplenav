/*
 * Id   : $Id$
 * This file is part of the SimpleNav project.
 * URL: https://sourceforge.net/projects/simplenav/ 
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * For further information about OpenCms, please see the
 * project website: http://www.opencms.org
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package net.sf.owt.simplenav.core;

import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.BodyContent;
import javax.servlet.jsp.tagext.BodyTagSupport;

/**
 * This is a helper class for building navigation lists. It loops through all <code>INavElement</code>s
 * provided by attribute or an surrounding <code>INavigationProvider</code> tag and constructs a List 
 * of <code>NavListEntry</code> objects. These objects are passed to the <code>renderOutput(List)</code> 
 * method wich must be overwritten by every implementation. The String return by this method is the 
 * text printed out by this tag. 
 * 
 * @author Sebastian Himberger
 */
public abstract class ANavListTag extends BodyTagSupport {

    /** Standard attribute for the navigation element **/
    public static String C_DEFAULT_ATTR_NAVELEM = "navElem";    
    
    /** Iterator over navigation elements **/
    private Iterator navIterator;
    
    /** The attribute used to store the current nav element **/
    private String attributeName;   

    /** List of navigation elements **/
    private List navigation;    
    
    /** List of evaluated body elements **/
    private List entries;    
    
    /**
     * Initializes default values and required objects
     */
    protected void doInitTag() {
    	entries = new LinkedList();
        setNavigation(findNavigation());   // find the correct navigation list
        
        // init attributeName
        if (attributeName == null) {
        	attributeName = HtmlNavListTag.C_DEFAULT_ATTR_NAVELEM;
        }
    }
    
    /**
     * Resets the tag members to take care of pooling
     */
    protected void resetTag() {
    	entries = null;
    	navigation = null;
    	attributeName = null;
    }
    
    /**
     * @see 
     */
    public int doStartTag() {
       	doInitTag();   				// init values            
        if (navigation != null) {   // if there's an navigation, loop through it
            navIterator = navigation.iterator();
            if (navIterator.hasNext()) {    
                // if there are elements in the navigation
                // make navigation element available to body
                pageContext.setAttribute(attributeName, navIterator.next()); 
                return EVAL_BODY_BUFFERED;
            }
        }
        // if no navigation is available do not evaluate body
        return SKIP_BODY;	        
    }
    
    /**
     * 
     */
    public int doAfterBody() throws JspException {
    	// get buffered body content
     	BodyContent body = getBodyContent();    
     	
     	// add evaluated body and current navElement to the list of navigation entries
     	entries.add(new NavListEntry(((INavElement)pageContext.getAttribute(attributeName)),body.getString()));
        
     	// handle the next navigation entry if there are any more
     	if (navIterator.hasNext()) {   
            pageContext.setAttribute(attributeName,navIterator.next());
            try {
                body.clear();
            } catch (IOException ioe) {
                throw new JspException("An IOException occured while clearing tag body " + ioe.getMessage() );
            }
            return EVAL_BODY_AGAIN;
        } 
        
        // complete navigation was iterated and all bodies stored
        try {
        	getPreviousOut().print(renderOutput(entries));
        } catch (IOException ioe) {
           throw new JspException("An IOException occured while printing out content " + ioe.getMessage());
        }
        
        resetTag();
        return SKIP_BODY;       
    }
    
    /** 
     * Finds the correct navigation list
     * 
     * @return List the navigation list either set by attribute or provided by a parent tag
     */
    private List findNavigation() {
        // look if we already have a valid navigation
        if (navigation == null) {
            // look for parent navigation providing tag
            INavigationProvider builderTag = (INavigationProvider) findAncestorWithClass(this, INavigationProvider.class);
            if (builderTag != null) {
                navigation = builderTag.provideNavigation();
            }
        }
        return navigation;
    }
    
    protected abstract String renderOutput(List navEntries); 
    
    /**
     * 
     * @param list
     */
    public void setNavigation(List list) {
        navigation = list;
    }
    
    /**
     * 
     * @return
     */
    public List getNavigation() {
        return navigation;
    }

    /**
     * 
     * @param name
     */
    public void setAttributeName(String name) {
        attributeName = name;
    }
    
    /**
     * 
     * @return
     */
    public String getAttributeName() {
        return attributeName;
    }
    
}
