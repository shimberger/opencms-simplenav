/*
 * Id   : $Id$
 * This file is part of the SimpleNav project.
 * URL: https://sourceforge.net/projects/simplenav/ 
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * For further information about OpenCms, please see the
 * project website: http://www.opencms.org
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package net.sf.owt.simplenav.core;

import java.util.Iterator;
import java.util.List;

/**
 * Tag for building a plain list of navigation elements.
 * This tag simply evaluates the body of itself and outputs
 * the result.
 * 
 * @author Sebastian Himberger
 */
public class PlainNavListTag extends ANavListTag {

    /**
     * @see net.sf.owt.simplenav.core.ANavListTag#renderOutput(java.util.List)
     */
    protected String renderOutput(List navEntries) {
        StringBuffer html = new StringBuffer(2048);
        NavListEntry navEntry;
       // loop through all navigation entries and output the evaluated body
        for (Iterator i = navEntries.iterator(); i.hasNext();) {
            navEntry = (NavListEntry) i.next();
            html.append(navEntry.getBody());
        }
        
        return html.toString();
    }

}
