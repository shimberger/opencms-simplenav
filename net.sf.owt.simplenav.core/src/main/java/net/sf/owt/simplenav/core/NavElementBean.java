/*
 * Id   : $Id$
 * This file is part of the SimpleNav project.
 * URL: https://sourceforge.net/projects/simplenav/ 
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * For further information about OpenCms, please see the
 * project website: http://www.opencms.org
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package net.sf.owt.simplenav.core;


/**
 * Simple POJO Bean wich implements <code>INavElement</code>.
 * 
 * @author Sebastian Himberger
 * @see net.sf.owt.simplenav.core.INavElement
 */
public class NavElementBean implements INavElement {

	/** The caption of this element */
	private String caption;
	
	/** Target url */
	private String url;
	
	/** The url of the navigation image */
	private String imageUrl;
	
	/** Navigation level */
	private int level;
	
	/** Is the currently selected resource */
	private boolean isActive;
	
	/** Is a Node/Folder */
	private boolean isNode;
	
	/** Lies on the path to the currently selected resource */
	private boolean isWaypoint;	
	
	/**
	 * @see net.sf.owt.simplenav.core.INavElement#getCaption()
	 */
	public String getCaption() {
		return caption;
	}

	/**
	 * @see net.sf.owt.simplenav.core.INavElement#setCaption(java.lang.String)
	 */
	public void setCaption(String caption) {
		this.caption = caption;
	}

	/**
	 * @see net.sf.owt.simplenav.core.INavElement#getUrl()
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @see net.sf.owt.simplenav.core.INavElement#setUrl(java.lang.String)
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * @see net.sf.owt.simplenav.core.INavElement#getImageUrl()
	 */
	public String getImageUrl() {
		return imageUrl;
	}

	/**
	 * @see net.sf.owt.simplenav.core.INavElement#setImageUrl(java.lang.String)
	 */
	public void setImageUrl(String url) {
		this.imageUrl = url;
	}	
	
	/**
	 * @see net.sf.owt.simplenav.core.INavElement#getLevel()
	 */
	public int getLevel() {
		return level;
	}

	/**
	 * @see net.sf.owt.simplenav.core.INavElement#setLevel(int)
	 */
	public void setLevel(int level) {
		this.level = level;
	}

	/**
	 * @see net.sf.owt.simplenav.core.INavElement#isNode()
	 */
	public boolean isNode() {
		return isNode;
	}

	/**
	 * @see net.sf.owt.simplenav.core.INavElement#setNode(boolean)
	 */
	public void setNode(boolean isNode) {
		this.isNode = isNode;
	}

	/**
	 * @see net.sf.owt.simplenav.core.INavElement#isWaypoint()
	 */
	public boolean isWaypoint() {
		return isWaypoint;
	}

	/**
	 * @see net.sf.owt.simplenav.core.INavElement#setWaypoint(boolean)
	 */
	public void setWaypoint(boolean isWaypoint) {
		this.isWaypoint = isWaypoint;
	}

	/**
	 * @see net.sf.owt.simplenav.core.INavElement#isActive()
	 */
	public boolean isActive() {
		return isActive;
	}

	/**
	 * @see net.sf.owt.simplenav.core.INavElement#setActive(boolean)
	 */
	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

}
