/*
 * Id   : $Id$
 * This file is part of the SimpleNav project.
 * URL: https://sourceforge.net/projects/simplenav/ 
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * For further information about OpenCms, please see the
 * project website: http://www.opencms.org
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package net.sf.owt.simplenav.core;

import java.util.Iterator;
import java.util.List;

/**
 * A tag which renders the CSS and JavaScript to make an ordinary
 * {@link HtmlNavListTag} into a CSS Dropdown menu as explained under:
 * 
 * <a href="http://www.htmldog.com/articles/suckerfish/">
 *  http://www.htmldog.com/articles/suckerfish/
 * </a>
 * 
 * @author Sebastian Himberger
 */
public class SuckerfishCSSDecoratorTag extends ANoBodyNavTag {

	/**
	 * Default css class which is used to fix IEs hovering behaviour
	 */
	public static final String DEFAULT_HOVER_CLASS = "sfHover";
	
	/**
	 * The width of a menu in CSS units
	 */
	private String width;
	
	/**
	 * The id of the list to decorate.
	 */
	private String listId;
	
	/**
	 * The currently used class to fix IE hover behaviour
	 */
	private String hoverClass = DEFAULT_HOVER_CLASS;
	
	/**
	 * @see ANoBodyNavTag#renderOutput(List)
	 */
	protected String renderOutput(List navEntries) {
		StringBuffer html = new StringBuffer();
		int startLevel = -1;
		int maxLevel = -1;
		// First determine start level and depth of the list
		for (Iterator i = navEntries.iterator(); i.hasNext();) {
			INavElement navElem = (INavElement) i.next();
			if (startLevel == -1) {
				startLevel = navElem.getLevel();
			}
			if (navElem.getLevel() > maxLevel) {
				maxLevel =navElem.getLevel();
			}
		}
		// delta = actual levels of the list
		int delta = maxLevel - startLevel;
		
		// create CSS
		html.append("<style type=\"text/css\">");
		basicStyles(html);
		if (delta >= 2) {			
			// If this is a multilevel dropdown generate the cascading
			// behaviour stuff
			for (int i = 2; i <= delta; i++) {
				html.append("#").append(listId).append(" li:hover ");
				appendMultipleTimes(html,"ul ",i);
				html.append(", #").append(listId).append(" li.").append(hoverClass).append(" ");
				appendMultipleTimes(html,"ul ",i);
				if (i < delta) {
					html.append(",");
				}
			}
			html.append(" { \n");
			html.append("  left: -999em;");
			html.append("}\n");
	
			for (int i = 0; i <= delta; i++) {
				html.append("#").append(listId).append(" ");
				appendMultipleTimes(html,"li ",i);
				html.append(" li:hover ul");
				html.append(", #").append(listId).append(" ");
				appendMultipleTimes(html,"li ",i);
				html.append(" li.").append(hoverClass).append(" ul");
				if (i < delta) {
					html.append(",");
				}
			}
			html.append(" { \n");
			html.append("  left: auto;");
			html.append("}\n");				
		}
		html.append("</style>");
		// generate the javascript for IE
		generateJavaScript(html);
		return html.toString();
	}
	
	/**
	 * Helper method to append a String multiple times.
	 * 
	 * @param html The html where to append the string
	 * @param string The string to append
	 * @param times The amount of appendices
	 */
	private void appendMultipleTimes(StringBuffer html,String string,int times) {
		for (int i = 0; i < times; i++) {
			html.append(string);
		}
	}

	/**
	 * Generate the Javascrip to fix IE.
	 * 
	 * @param html The html buffer where to append the JS
	 */
	private void generateJavaScript(StringBuffer html) {
		html.append("<script type=\"text/javascript\"><!--//--><![CDATA[//><!--)\n");
		html.append("sfHover"+listId+" = function() {\n");
		html.append("	var sfEls = document.getElementById(\""+listId+"\").getElementsByTagName(\"LI\");\n");
		html.append("	for (var i=0; i<sfEls.length; i++) {\n");
		html.append("		sfEls[i].onmouseover=function() {\n");
		html.append("			this.className+=\" "+ hoverClass +"\";\n");
		html.append("		}\n");
		html.append("		sfEls[i].onmouseout=function() {\n");
		html.append("			this.className=this.className.replace(new RegExp(\" "+ hoverClass +"\\\\b\"), \"\");\n");
		html.append("		}\n");
		html.append("	}\n");
		html.append("}\n");
		html.append("if (window.attachEvent) window.attachEvent(\"onload\", sfHover"+listId+");\n");
		html.append("//--><!]]></script>\n");
	}

	/**
	 * Append the general style definitions (regardless of this is multilevel
	 * or singlelevel).
	 * 
	 * @param html The html buffer where to append the styles.
	 */
	private void basicStyles(StringBuffer html) {
		html.append("#").append(listId).append(", #").append(listId).append(" ul {\n");
		html.append("  padding: 0;\n");
		html.append("  margin: 0;\n");
		html.append("  list-style: none;\n");
		// The following is needed for multilevel dropdowns
		html.append("  line-height: 1;\n");
		html.append("}\n\n");;
		
		html.append("#").append(listId).append(" a {\n");
		html.append("  display: block;\n");
		html.append("  width: ").append(width).append(";\n");
		html.append("}\n\n");

		html.append("#").append(listId).append(" li {\n");
		html.append("  float: left;\n");
		html.append("  width: ").append(width).append(";\n");
		html.append("}\n");
		
		html.append("#").append(listId).append(" li ul {\n");
		html.append("  position: absolute;\n");
		html.append("  width: ").append(width).append(";\n");
		html.append("  left: -999em;\n");
		html.append("}\n\n");
		
		// Needed for multilevel dropdowns
		html.append("#").append(listId).append(" li ul ul {\n");
		html.append("  margin: -1em 0 0 ").append(width).append("\n");
		html.append("}\n\n");

		html.append("#").append(listId).append(" li:hover ul, ");
		html.append("#").append(listId).append(" li.").append(hoverClass).append(" ul {");
		html.append("  left: auto;");
		html.append("}\n\n");
		
	}
	
	/**
	 * Sets the width of the menu in CSS units.
	 * 
	 * @param width The width of the menu in CSS units
	 */
	public void setWidth(String width) {
		this.width = width;
	}
	
	/**
	 * Returns the currently used with.
	 * 
	 * @return The currently used with-
	 */
	public String getWidth() {
		return this.width;
	}

	/**
	 * Gets the id of the htmlList to decorate.
	 * 
	 * @return The ID of the htmlList.
	 */
	public String getListId() {
		return listId;
	}

	/**
	 * Sets the id of the htmlList to decorate.
	 * 
	 * @param listId The ID of the htmlList.
	 */
	public void setListId(String listId) {
		this.listId = listId;
	}
	
}
