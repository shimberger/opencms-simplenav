/*
 * Id   : $Id$
 * This file is part of the SimpleNav project.
 * URL: https://sourceforge.net/projects/simplenav/ 
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * For further information about OpenCms, please see the
 * project website: http://www.opencms.org
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package net.sf.owt.simplenav.core;

/**
 * An interface for a generic navigation element. It represents a single entry
 * in a navigation having some attributes for the most common navigations.
 * 
 * @author Sebastian Himberger
 */
public interface INavElement {

	/**
	 * The caption is the label/title used for this navigation element
	 * 
	 * @return the caption of this element
	 */
	public String getCaption();
	
	/**
	 * @see net.sf.owt.simplenav.core.INavElement#getCaption()
	 * @param caption The caption of this navigation element
	 */
	public void setCaption(String caption);
	
	/** 
	 * @return the url assosiated with this element
	 */
	public String getUrl();
	
	/**
	 * @param url The url of this navigation element
	 */
	public void setUrl(String url);

	/**
	 * The Url of the image associated with this element. This is of
	 * course only usefull for graphical navigations.
	 * 
	 * @return The image url
	 */
	public String getImageUrl();
	
	/**
	 * @see net.sf.owt.simplenav.core.INavElement#getImageUrl()
	 * @param url The image url
	 */
	public void setImageUrl(String url);
	
	/**
	 * The level or how deep this element is found in the navigation tree.
	 * The levels should be 0 index meaning in the path /folder/element.html
	 * folder is level 0 and element level 1. Just because informatics love
	 * 0-indexed so much ;)
	 * 
	 * @return the Navigation level
	 */
	public int getLevel();
	
	/**
	 * @see net.sf.owt.simplenav.core.INavElement#getLevel()
	 * @param level The navigation level
	 */
	public void setLevel(int level);
	
	/**
	 * Returns <code>true</code> if this element is a ode e.g. a folder.
	 * 
	 * @return <code>true</code> if element is a node
	 */
	public boolean isNode();
	
	/**
	 * @see net.sf.owt.simplenav.core.INavElement#isNode()
	 * @param isNode <code>true</code> if element is a node
	 */
	public void setNode(boolean isNode);
	
	/**
	 * An element is declared a waypoint if it lies on the path to the
	 * current location in the navigation. E.g. if the current location is
	 * <code>/site/section/java/tutorial.html</code> the <code>section</code>
	 * element is a waypoint on the way towards tutorial.html
	 * 
	 * @return <code>true</code> if element is a waypoint
	 */
	public boolean isWaypoint();
	
	/**
	 * @see net.sf.owt.simplenav.core.INavElement#isWaypoint()
	 * @param isWaypoint <code>true</code> if element is a waypoint
	 */
	public void setWaypoint(boolean isWaypoint);
	
	/**
	 * The active element is the current location in the navigation.
	 * E.g. if the position in a navigation is <code>/site/section/java/tutorial.html</code>
	 * <code>tutorial.html</code> is the currently active element.
	 * 
	 * @return <code>true</code> If the element is active
	 */
	public boolean isActive();
	
	/**
	 * @see net.sf.owt.simplenav.core.INavElement#isActive()
	 * @param isActive <code>true</code> If the element is active
	 */
	public void setActive(boolean isActive);
	
}
