/*
 * Id   : $Id$
 * This file is part of the SimpleNav project.
 * URL: https://sourceforge.net/projects/simplenav/ 
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * For further information about OpenCms, please see the
 * project website: http://www.opencms.org
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package net.sf.owt.simplenav.core;

import java.io.IOException;
import java.util.List;

import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.tagext.TagSupport;

/**
 * Base class for tags without body (e.g. decorator tags). 
 * 
 * @author Sebastian Himberger
 */
public abstract class ANoBodyNavTag extends TagSupport {

    /** List of navigation elements **/
    private List navigation;    

    /**
     * Initializes default values and required objects
     */
    protected void doInitTag() {
        setNavigation(findNavigation());   // find the correct navigation list       
    }
    
    /**
     * @see TagSupport#doEndTag()
     */
    public int doEndTag() throws JspTagException {
       	doInitTag();   				// init values            
	    if (navigation != null) {   // if there's an navigation, loop through it
	    	String html = renderOutput(getNavigation());
	        try {
	        	pageContext.getOut().print(html);
	        } catch (IOException ioe) {
	        	throw new JspTagException("An IOException occured while printing out content " + ioe.getMessage());
			}
	    }
	    return EVAL_PAGE;
    }
    
    /**
     * Resets the tag members to take care of pooling
     */
    protected void resetTag() {
    	navigation = null;
    }
    
    /**
     * Method to render the HTML.
     * 
     * @param navigation A <code>List</code> of <code>INavElement</code> objects.
     * @return The rendered HTML. Never <code>null</code>
     */
    protected abstract String renderOutput(List navigation); 
    
     /** 
     * Finds the correct navigation list
     * 
     * @return List the navigation list either set by attribute or provided by a parent tag
     */
    private List findNavigation() {
        // look if we already have a valid navigation
        if (navigation == null) {
            // look for parent navigation providing tag
            INavigationProvider builderTag = (INavigationProvider) findAncestorWithClass(this, INavigationProvider.class);
            if (builderTag != null) {
                navigation = builderTag.provideNavigation();
            }
        }
        return navigation;
    }

    /**
     * Returns the currently used navigation.
     * 
     * @return A <code>List</code> of <code>INavElement</code> objects.
     */
	public List getNavigation() {
		return navigation;
	}

	/**
	 * Sets the currently used navigation.
	 * 
	 * @param navigation A <code>List</code> of <code>INavElement</code> objects.
	 */
	public void setNavigation(List navigation) {
		this.navigation = navigation;
	}    
	
}
