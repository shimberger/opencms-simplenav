/*
 * Id   : $Id$
 * This file is part of the SimpleNav project.
 * URL: https://sourceforge.net/projects/simplenav/ 
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * For further information about OpenCms, please see the
 * project website: http://www.opencms.org
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package net.sf.owt.simplenav.core;

import java.util.List;

/**
 * Tag for building a HtmlList from an List of navigation Objects.
 * 
 * <b>Example:</b>
 * 	<ul>
 *		<li>element A<ul>
 *			<li>subelement I</li>
 *			<li>subelement II</li>
 *			<li>subelement III</li>
 *		</ul></li>
 *		<li>element B</li>
 *	</ul>
 * 
 * You can control the rendering of the html elements (&lt;ul&gt;,&lt;li&gt;,...) by extending
 * this class and override the appropriate renderXXX methods. See the method description
 * for further details.
 * 
 * @author Sebastian Himberger
 */
public class HtmlNavListTag extends ANavListTag  {
	
	/** The HTML-id of the main list */
	private String listId;
	
	/** The HTML-class of the main list */
	private String listClass;
	
	/** The HTML-class of the nested lists */
	private String nestedListClass;
	
	protected void doInitTag() {
		super.doInitTag();
	}
	
	/**
	 * Loops through the nav Elements and calls the appropriate
	 * renderXXX method if html has to be rendered. If you want to
	 * change the outputted html code have a look at the following
	 * methods.
	 *
	 * @see net.sf.owt.simplenav.core.HtmlNavListTag#renderListBegin(INavElement)
	 * @see net.sf.owt.simplenav.core.HtmlNavListTag#renderNestedListBegin(INavElement)
	 * @see net.sf.owt.simplenav.core.HtmlNavListTag#renderListEnd()
	 * @see net.sf.owt.simplenav.core.HtmlNavListTag#renderItemBegin(INavElement)
	 * @see net.sf.owt.simplenav.core.HtmlNavListTag#renderItemEnd()
	 * @return The HTML code printed by this tag.
	 */
	protected String renderOutput(List navEntries) {
		StringBuffer result = new StringBuffer(2048);
		INavElement navElem;
		NavListEntry navEntry;
		String body;
		int oldLevel = -1;
		int startLevel = -1;
		
		for (java.util.Iterator i = navEntries.iterator(); i.hasNext();) {    	
			navEntry = (NavListEntry) i.next();
			navElem = navEntry.getNavElement();
			body = navEntry.getBody();
			int level = navElem.getLevel();
			
			if (level != -1 && startLevel == -1) {
				startLevel = level;
			}
			
            if (oldLevel != -1) {
                // manage level transitions
                if (level == oldLevel) {
                    // same level, close only previous list item
                    result.append(renderItemEnd() + "\n");
                } else if (level < oldLevel) {
                    // lower level transition, determine delta
                    int delta = oldLevel - level;
                    for (int k = 0; k < delta; k++) {
                        // close sub list and list item
                        result.append(renderItemEnd() + "\n" + renderListEnd() + "\n");
                    }
                    result.append(renderItemEnd() + "\n");
                } else {
                    // higher level transition, create new sub list
                    result.append(renderNestedListBegin(navElem) + "\n");
                }
            } else {
                // initial list creation
                result.append(renderListBegin(navElem) + "\n");
            }
			result.append(renderItemBegin(navElem) + body);
            oldLevel = level;

		}	
        for (int i = startLevel; i < oldLevel+1; i++) {
            // close the remaining lists
            result.append(renderItemEnd() + renderListEnd() + "\n");
        }			
		return result.toString();
	}
	
	/**
	 * Renders the main list begin. This method is only called once
	 * for the main navigation list. The nested lists are rendered
	 * using <code>renderNestedListBegin(INavElement)</code>.
	 * 
	 * @param navElem The current <code>INavElement</code>
	 * @return The HTML code for the List begin
	 */
	protected String renderListBegin(INavElement navElem) {
		return "<ul" + getClassAttr(listClass) + getIdAttr(listId) + ">";
	}
	
	/**
	 * Renders a nested List beginning.
	 * 
	 * @param navElem The current <code>INavElement</code>
	 * @return The HTML code for the List begin
	 */
	protected String renderNestedListBegin(INavElement navElem) {
		return "<ul " + getClassAttr(nestedListClass) + " >";	}
	
	/**
	 * Renders a List ending.
	 * 
	 * @return The HTML code for the List ending
	 */
	protected String renderListEnd() {
		return "</ul>";
	}
	
	/**
	 * Renders the beginning of a single list item
	 * 
	 * @param navElem The current <code>INavElement</code>
	 * @return The HTML code for the Item beginning
	 */
	protected String renderItemBegin(INavElement navElem) {
		return "<li>";
	}
	
	/**
	 * Renders the ending of a single list item
	 * 
	 * @return The HTML code for the Item ending
	 */
	protected String renderItemEnd() {
		return "</li>";
	}

	protected String getClassAttr(String cssClass) {
		if (cssClass != null && cssClass != "") {
			return " class=\"" + cssClass + "\" ";
		}
		return "";
	}
	
	protected String getIdAttr(String htmlId) {
		if (htmlId != null && htmlId != "") {
			return " id=\"" + htmlId + "\" ";
		}
		return "";
	}	
	
	/**
	 * @return Returns the listClass.
	 */
	public String getListClass() {
		return listClass;
	}
	/**
	 * @param listClass The listClass to set.
	 */
	public void setListClass(String listClass) {
		this.listClass = listClass;
	}
	/**
	 * @return Returns the listId.
	 */
	public String getListId() {
		return listId;
	}
	/**
	 * @param listId The listId to set.
	 */
	public void setListId(String listId) {
		this.listId = listId;
	}
	/**
	 * @return Returns the nestedListClass.
	 */
	public String getNestedListClass() {
		return nestedListClass;
	}
	/**
	 * @param nestedListClass The nestedListClass to set.
	 */
	public void setNestedListClass(String nestedListClass) {
		this.nestedListClass = nestedListClass;
	}
}
