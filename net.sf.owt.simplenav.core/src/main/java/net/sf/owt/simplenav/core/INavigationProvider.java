/*
 * Id   : $Id$
 * This file is part of the SimpleNav project.
 * URL: https://sourceforge.net/projects/simplenav/ 
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * For further information about OpenCms, please see the
 * project website: http://www.opencms.org
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package net.sf.owt.simplenav.core;

import java.util.List;

/**
 * An implementation of this class must provide a List of <code>INavElement</code>s,
 * in the correct order. If not all attributes of the <code>INavElement</code> interface
 * are used this should be mentioned in the documentation of the implementation.
 * 
 * @see net.sf.owt.simplenav.core.INavElement
 * @author Sebastian Himberger
 */
public interface INavigationProvider {

    /**
     * Method to access the navigation List of this implementation.
     * Mostly a List of <code>INavElements</code>
     * 
     * @see net.sf.owt.simplenav.core.INavElement
     * @return The navigation list provided by this implementation
     */
	public List provideNavigation();
	
}
