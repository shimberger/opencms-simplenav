/*
 * Id   : $Id$
 * This file is part of the SimpleNav project.
 * URL: https://sourceforge.net/projects/simplenav/ 
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * For further information about OpenCms, please see the
 * project website: http://www.opencms.org
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package net.sf.owt.simplenav.core;

import java.util.Iterator;
import java.util.List;

/**
 * Tag for buidling a navigation list where all entries are
 * seperated by a string wich is given using an attribute.
 * 
 * @author Sebastian Himberger
 */
public class SeperatedNavListTag extends ANavListTag {

    /**
     * The String wich is used to seperate the elements
     * of the navigation
     */
    private String seperator;
    
    /** 
     * @see net.sf.owt.simplenav.core.ANavListTag#renderOutput(java.util.List)
     */
    protected String renderOutput(List navEntries) {
        StringBuffer html = new StringBuffer(2048);
        NavListEntry navEntry;
        boolean isFirstEntry = true;
        // loop through all navigation elements
        for (Iterator i = navEntries.iterator(); i.hasNext();) {
            // prepend the seperator to all nav elements except the first
            if (!isFirstEntry) {
                html.append(seperator);
            }
            isFirstEntry = false;
            navEntry = (NavListEntry) i.next();
            html.append(navEntry.getBody());
        }
        
        return html.toString();
    }
    
    /**
     * @param string The String to seperate the elements
     */
    public void setSeperator(String string) {
        seperator = string;
    }
    
    /**
     * @return The seperating string
     */
    public String getSeperator() {
        return seperator;
    }
    

}
